FROM centos:7

COPY ./sshd_config /etc/ssh/
COPY ./config /etc/selinux/
RUN rm /etc/yum.repos.d/*
COPY ./local.repo /etc/yum.repos.d/

CMD ["/usr/sbin/init"]
