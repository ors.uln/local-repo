# Локальный репозиторий в докер контейнере CentOS

1-ый контейнер предоставляет функцию RPM репозитория.
2-ой контейнер являясь клиентом этого репозитория, может устанавливать из него пакеты.

1-ый контейнер:
- возможность добавление/удаление  пакетов в репозитории по ssh по ключу извне
- возможность использования уже готового репозитория (готовой директории с пакетами)
- автоматическая проверка доступности репозитория
- вывод всех логов через коллектор логов докера

2-ой контейнер:
- возможность автоматической установки пакетов из репозитория 1-ого контейнера


## Что бы запустить надо выполнить

```
docker compose up -d
```

## Что бы подключиться по ssh к 1-ому контейнеру

```
ssh repadm@127.0.0.1 -p 77 -i id_rsa
```
