FROM centos:7

COPY ./config /etc/selinux/

RUN yum install -y epel-release \
    && yum install -y nginx createrepo yum-utils openssh-server openssh-clients  \
    && yum clean all \
    && ssh-keygen -A \
    && mkdir -p /var/www/repo/centos

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && ln -sf /dev/stderr /var/log/sshd.log \
    && useradd repadm -m \
    && chmod -R g+w /var/www/repo \
    && chown -R repadm:repadm /var/www/repo \
    && usermod -aG repadm nginx

COPY ./sshd_config /etc/ssh/
COPY ./id_rsa.pub /home/repadm/.ssh/authorized_keys

RUN chmod 700 /home/repadm/.ssh \
    && chmod 644 /home/repadm/.ssh/authorized_keys \
    && chown -R repadm:repadm /home/repadm/.ssh
COPY ./repo.conf /etc/nginx/conf.d/

EXPOSE 22 80

CMD /usr/sbin/sshd -E /var/log/sshd.log ; nginx -g "daemon off;"

